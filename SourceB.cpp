#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	string s = "select * from accounts where Buyer_id=" + to_string(buyerid) + " and balance>=(select price from cars where id=" + to_string(carid) + ") and (select available from cars where id=" + to_string(carid) + ")=1";
	const char* p_c_str = s.c_str();
	cout << s << endl;
	clearTable();
	rc = sqlite3_exec(db, p_c_str, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK || results.empty())
	{
		if (rc != SQLITE_OK) {
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
		}
		return false;
	}
	s = "update accounts set balance=balance-(select price from cars where id=" + to_string(carid) + ") where Buyer_id=" + to_string(buyerid);
	p_c_str = s.c_str();
	cout << s << endl;
	clearTable();
	rc = sqlite3_exec(db, p_c_str, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		if (rc != SQLITE_OK) {
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
		}
		return false;
	}
	s = "select * from accounts where id" + to_string(carid);
	p_c_str = s.c_str();
	cout << s << endl;
	clearTable();
	rc = sqlite3_exec(db, p_c_str, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	else
	{
		printTable();
	}
	return true;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string s;
	const char* p_c_str;
	cout << "begin transaction" << endl;
	rc = sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	s = "update accounts set balance=balance+" + to_string(amount) + " where id =" + to_string(to);
	p_c_str = s.c_str();
	cout << s << endl;
	rc = sqlite3_exec(db, p_c_str, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	s = "update accounts set balance=balance-" + to_string(amount) + " where id =" + to_string(from);
	p_c_str = s.c_str();
	cout << s << endl;
	rc = sqlite3_exec(db, p_c_str, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	cout << "commit" << endl;
	rc = sqlite3_exec(db, "commit", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	cout << "select * from accounts" << endl;
	clearTable();
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	else
	{
		printTable();
	}
	return true;
}

void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	int rc;
	string s = "select * from accounts where balance>=(select price from cars where id=" + to_string(carId) + ")";
	const char* p_c_str = s.c_str();
	cout << s << endl;
	clearTable();
	rc = sqlite3_exec(db, p_c_str, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	else
	{
		printTable();
	}
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	rc = sqlite3_open("C:\\Users\\GIGABYTE\\Documents\\Study\\Magshimim\\CPP\\SemesterB\\Assignment3\\2\\carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	string s = "select color, count(*) from cars group by color";
	const char* p_c_str = s.c_str();
	cout << s << endl;
	rc = sqlite3_exec(db, p_c_str, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	else
	{
		printTable();
	}

	return 0;
}